import yaml
import json
import os
import re
import pwd
import time
import datetime
import discord
import asyncio
from apscheduler.schedulers.asyncio import AsyncIOScheduler

global config
client = discord.Client()
scheduler = AsyncIOScheduler()
scheduled_jobs = []

config = {
}

def load_config():
    print('Loading configuration...')
    t0 = time.time()
    try:
        
        with open('config.yaml') as cfg:
            config['main'] = yaml.safe_load(cfg)
        print('Main configuration loaded.')

        for name in os.listdir('config'):
            if name.startswith('config') and name.endswith('yaml'):
                server_id = re.sub(r"\D", "", name)
                with open('config/' + name) as cfg:
                    config[server_id] = yaml.safe_load(cfg)
                    print("Loaded configuration for server " + server_id + ".")
        message = 'All configs loaded! Took ' + str(time.time() - t0) + ' seconds.'
    except Exception as e:
        message = 'There was an error loading the configurations. {0}'.format(e)
    print(message)
    return message


print('jerbot, a multi-purpose moderation bot')

load_config()


@client.event
async def on_message(message):
    global config
    server_id = message.server.id

    # Logging
    with open('log_{0}.txt'.format(server_id), 'a+') as log:
        log.write(message.id + ' | ' + message.author.name + ' (' + message.author.id + ') | ' + message.content + '\n')

    # Owner commands
    if message.author.id == config['main']['owner_id']:
        if message.content == config[server_id]['prefix'] + 'reloadcfg':
            await client.delete_message(message)
            delmsg = await client.send_message(message.channel, load_config())
            await asyncio.sleep(10)
            await client.delete_message(delmsg)

    # Blacklists
    override_blacklist = False
    for feature in config[server_id]['features']:
        if not feature['feature'] == 'blacklist' or message.author == client.user or not feature['enabled']:
            pass
        else:
            for role in message.author.roles:
                for exempt_role in feature['overrides']:
                 if role.name.lower() == exempt_role.lower():
                        override_blacklist = True
            if override_blacklist:
                pass
            else:
                for deletion in feature['deletions']:
                    for trigger in deletion['trigger']:
                        if trigger in re.sub(r'\s+', '', message.content.lower()):
                            await client.delete_message(message)
                            await client.send_message(message.author, deletion['response'] + config[server_id]['footer'])
                            logmsg = '**Blacklisted phrase:** <@{id}> {log} in <#{channel_id}>'.format(id=message.author.id, channel_id=message.channel.id, log=deletion['log'])
                            # embed = discord.Embed(color=0x730303, title=message.content)
                            await client.send_message(client.get_channel(str(config[server_id]['modlog_id'])), logmsg)
        
    # General-use commands
    if message.author == client.user or not message.content.startswith(config[server_id]['prefix']):
        return

    commands = []

    for feature in config[server_id]['features']:
        if feature['enabled']:
            if 'command_name' in feature:
                commands.append(feature['command_name'])
            if feature['feature'] == 'probation':
                commands.append("un" + feature['command_name'])
                commands.append("auto" + feature['command_name'])
            if feature['feature'] == 'public_roles':
                commands.append(feature['leave_name'])

    valid = False     
    for command in commands:
        if message.content.startswith(config[server_id]['prefix'] + command):
            valid = True

    if not valid:
        return

    match_found = False
    delmsg_override = False
    await client.delete_message(message)
    for feature in config[server_id]['features']:
        auth = False
        if not match_found:
            if 'command_roles' in feature:
                if not feature['command_roles']:
                    auth = True
                else:
                    for role in message.author.roles:
                        for auth_role in feature['command_roles']:
                            if role.name.lower() == auth_role.lower():
                                    auth = True
            if not auth:
                delmsg = "<@{0}>, you don't have permission to use this command.".format(message.author.id)
            else:
                try:
                    if feature['feature'] == 'probation' and feature['enabled']:
                        if message.content.startswith(config[server_id]['prefix'] + feature['command_name']):
                            match_found = True
                            user = message.content[len(message.content.split(' ')[0]) + 1:]
                            userID = user.replace('<', '').replace('>', '').replace('!', '').replace('@', '')
                            await client.add_roles(discord.utils.get(message.server.members, id=userID), discord.utils.get(message.server.roles, name=feature['role_name']))
                            delmsg = "{0} is now in {1}.".format(user, feature['role_name'].lower())
                            with open('probations_{0}.txt'.format(server_id), 'a+') as probations:
                                probations.write(userID + '\n')

                        elif message.content.startswith(config[server_id]['prefix'] + 'un' + feature['command_name']):
                            match_found = True
                            user = message.content[len(message.content.split(' ')[0]) +1:]
                            userID= user.replace('<', '').replace('>', '').replace('!', '').replace('@', '')
                            await client.remove_roles(discord.utils.get(message.server.members, id=userID), discord.utils.get(message.server.roles, name=feature['role_name']))
                            delmsg = "{0} is no longer in {1}.".format(user, feature['role_name'].lower())
                            with open('probations_{0}.txt'.format(server_id), 'a+') as probations:
                                users = probations.readlines()
                                probations.seek(0)
                                for user in users:
                                    if user != userID + "\n":
                                        probations.write(user)
                                        probations.truncate()

                        elif message.content.startswith(config[server_id]['prefix'] + 'auto' + feature['command_name']):
                            match_found = True
                            feature['auto'] = ~feature['auto']
                            if feature['auto']:
                                delmsg = 'Auto-{0} has been **enabled.**'.format(feature['role_name'].lower())
                                await client.send_message(client.get_channel(str(config[server_id]['modlog_id'])), 'Auto-{0} has been **enabled.**'.format(feature['role_name'].lower()))
                            else:
                                delmsg = 'Auto-{0} has been **disabled.**'.format(feature['role_name'].lower())
                                await client.send_message(client.get_channel(str(config[server_id]['modlog_id'])), 'Auto-{0} has been **disabled.**'.format(feature['role_name'].lower()))

                    elif feature['feature'] == 'getinfo' and feature['enabled']:
                        if message.content.startswith(config[server_id]['prefix'] + feature['command_name']):
                            match_found = True
                            delmsg_override = True
                            user = message.content[len(message.content.split(' ')[0]) +1:]
                            userID= user.replace('<', '').replace('>', '').replace('!', '').replace('@', '')
                            # TODO add to embed / beautify
                            await client.send_message(message.channel, '<@{0}> created at **{1} UTC** '.format(userID, discord.utils.get(message.server.members, id=userID).created_at))
                            
                    elif feature['feature'] == 'public_roles' and feature['enabled']:
                        roles = []
                        requested_role = message.content[len(message.content.split(' ')[0]) +1:]
                        for role in feature['public_roles']:
                            roles.append(role)

                        if message.content.startswith(config[server_id]['prefix'] + feature['command_name']):
                            match_found = True
                        #TODO Implement role groups- mutually exclusive roles
                            for role in roles:
                                if requested_role.lower() == role.lower():
                                    await client.add_roles(discord.utils.get(message.server.members, id=message.author.id), discord.utils.get(message.server.roles, name=role))
                                    delmsg = "Done!"
                            
                            if not delmsg == "Done!":
                                delmsg_override = True
                                await client.send_message(message.channel, "Sorry, I didn't recognize that role! Available roles are:\n`{0}`".format(', '.join(roles)))

                        elif message.content.startswith(config[server_id]['prefix'] + feature['leave_name']):
                            match_found = True
                            for role in roles:
                                if requested_role.lower() == role.lower():
                                    await client.remove_roles(discord.utils.get(message.server.members, id=message.author.id), discord.utils.get(message.server.roles, name=role))
                                    delmsg = "Done!"
                            
                                if not delmsg == "Done!":
                                    delmsg_override = True
                                    await client.send_message(message.channel, "Sorry, I didn't recognize that role! Available roles are:\n`{0}`".format(', '.join(roles)))

                # ----------------------------
                except Exception as e:
                    delmsg = "<@{0}>, an error occured while processing the command. ({1})".format(message.author.id, e)
    if 'delmsg' in locals() and not delmsg_override:
        delmsg = await client.send_message(message.channel, delmsg)
        await asyncio.sleep(10)
        await client.delete_message(delmsg)   


@client.event
async def on_message_edit(oldmsg, newmsg):

    global config
    server_id = newmsg.server.id

    with open('log_{0}.txt'.format(server_id), 'a+') as log:
        log.write(newmsg.id + ' (edited) | ' + newmsg.author.name + ' (' + newmsg.author.id + ') | ' + newmsg.content + '\n')

    # Re-check blacklists
    override_blacklist = False
    for feature in config[server_id]['features']:
        if not feature['feature'] == 'blacklist':
            pass
        elif newmsg.author == client.user or not feature['enabled']:
            pass
        else:
            for role in newmsg.author.roles:
                for exempt_role in feature['overrides']:
                 if role.name.lower() == exempt_role.lower():
                        override_blacklist = True
            if override_blacklist:
                pass
            else:
                for deletion in feature['deletions']:
                    for trigger in deletion['trigger']:
                        if trigger in re.sub(r'\s+', '', newmsg.content.lower()):
                            await client.delete_message(newmsg)
                            await client.send_message(newmsg.author, deletion['response'] + config[server_id]['footer'])
                            logmsg = '**Blacklisted phrase:** <@{id}> {log} in <#{channel_id}> \nAdditionally, a **filter bypass attempt** was detected (user edited blacklisted phrase into an old message).'.format(id=newmsg.author.id, channel_id=newmsg.channel.id, log=deletion['log'])
                            await client.send_message(client.get_channel(str(config[server_id]['modlog_id'])), logmsg)

@client.event
async def on_member_join(member):
    server_id = member.server.id

    for feature in config[server_id]['features']:
        if feature['feature'] == 'greeting' and feature['enabled']:
            await client.send_message(member, 'Welcome to the {0} server, {1}! {2}'.format(member.server.name, member.mention, feature['message'] + config[server_id]['footer']))

        elif feature['feature'] == 'join_leave_logs' and feature['enabled']:
            embed = discord.Embed(color=0x398ED6, title="A user has joined.")
            embed.add_field(name="-----", value="User {name}#{discrim} with ID {id} has joined the server.".format(name=member.name, discrim=member.discriminator, id=member.id))
            embed.set_thumbnail(url=member.avatar_url)
            await client.send_message(client.get_channel(str(config[server_id]['modlog_id'])), embed=embed)   

        elif feature['feature']  == 'probation' and feature['enabled']:
            with open('probations_{0}.txt'.format(server_id), 'a+') as probations:
                probations.seek(0)
                users = probations.readlines()
                if feature['auto']:
                    probations.write(member.id + '\n')
                    await client.add_roles(member, discord.utils.get(member.server.roles, name=feature['role_name']))
                    await client.send_message(client.get_channel(str(config[server_id]['modlog_id'])), "**NOTE:** {0} has joined while auto-{1} is enabled. Role automatically placed on user.".format(member.mention, feature['role_name']))
                    with open("probations.txt", "r+") as probations:
                        users = probations.readlines()
                        probations.seek(0)
                        for user in users:
                           if user != userID + "\n":
                                probations.write(user)
                                probations.truncate()

                for user in users:
                    if user == member.id + "\n":
                        try:
                            await client.add_roles(member, discord.utils.get(member.server.roles, name=feature['role_name']))
                        except Exception as e:
                            await client.send_message(client.get_channel(str(config[server_id]['modlog_id'])), "An error occured while placing {0} on this user. ({1})".format(feature['role_name'], e))
                        else:
                            await client.send_message(client.get_channel(str(config[server_id]['modlog_id'])), "**NOTE:** {0} has attempted a {1} bypass. Role automatically placed on user.".format(member.mention, feature['role_name']))


@client.event
async def on_member_remove(member):
    server_id = member.server.id

    for feature in config[server_id]['features']:
        if feature['feature'] == 'join_leave_logs' and feature['enabled']:
            embed = discord.Embed(color=0xD63939, title="A user has left.")
            embed.add_field(name="-----", value="User {name}#{discrim} with ID {id} has left the server. They had the nickname of {nick}.".format(name=member.name, discrim=member.discriminator, id=member.id, nick=member.nick))
            embed.set_thumbnail(url=member.avatar_url)
            await client.send_message(client.get_channel(str(config[server_id]['modlog_id'])), embed=embed)

@client.event
async def on_reaction_add(reaction, user):
    server_id = user.server.id

    for feature in config[server_id]['features']:
        list_pass = False
        if feature['feature'] == 'autopin' and feature['enabled']:

            if feature['whitelist']:
                for channel in feature['list']:
                    if reaction.message.channel.id == str(channel):
                        list_pass = True

            elif not feature['whitelist']: # blacklist
                list_pass = True
                for channel in feature['list']:
                    if reaction.message.channel.id == str(channel):
                        list_pass = False

            if not list_pass:
                return 

            authorized = True

            for role in user.roles:
                for blacklisted_role in feature['blacklisted_roles']:
                 if role.name.lower() == blacklisted_role.lower():
                        authorized = False

            if not authorized or user == reaction.message.author:
                await client.remove_reaction(reaction.message, reaction.emoji, user)

            if reaction.emoji == "📌" and reaction.count >= feature['threshold'] and not reaction.message.pinned:
                await client.pin_message(reaction.message)

async def say_message(message, channel):
    print("Sending pre-determined message {0}".format(message))
    await client.send_message(client.get_channel(str(channel)), message)

for server in config:
    if bool(re.search(r'\d', server)):
        for feature in config[server]['features']:
            if feature['feature'] == 'automsg' and 'enabled' in feature and feature['enabled']:
                for message in feature['messages']:
                    for rawtime in message['time']:
                        scheduler.add_job(say_message, 'cron', day_of_week = rawtime['day'], hour = rawtime['hour'], minute = rawtime['minute'], second = rawtime['second'], args=[message['message'], message['channel']])
                    

scheduler.start()

@client.event
async def on_ready():
    print('Ready! Logged in as ' + client.user.name)
    print('----------')

client.run(config['main']['token'])

