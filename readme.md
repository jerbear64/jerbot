# jerbot
## Multi-server moderation bot

**jerbot** is the combination of two other moderation bots that I have written in the past- [99LBot](https://gitlab.com/jerbear64/99lbot), and a fork of 99LBot for the [NDS(i)Brew Scene Discord server](https://discord.gg/w4SKAr8). The features of these two bots were unified in order to make a monolithic bot that has proper multi-server support.

## Features

* Configurable commands
    - No command names are hardcoded, are server specific, and can be configured without restarting the bot
    - Commands can be locked behind specific roles if desired

* Probation system
    - Locks users behind a probation role
    - Takes note of the user ID, preventing a user from escaping probation by leaving and rejoining the server
    - Has an auto-probation mode, putting all new users in probation by default in the event of raids

* Join/leave logs
    - Marks account age and ID, as well as nickname upon leaving

* Info command
    - Can get info about an account on-demand, including creation date and the date on which they joined the server

* Blacklist
    - Scans messages for a blacklisted word or phrase, automatically deletes it, reports the infraction in a log channel, and DM's the user
    - Can be updated without restarting the bot

* Greeting
    - DM's the user when they join the server

* Public roles
    - Allows users to add public roles to themselves, eliminating the need to bother a moderator to do it

* Automatic pinning
    - Pins messages when a specified reaction on a message hits a specified amount of reactions
    - Useful for things like art channels
    - Has a white/blacklist built in, stopping the command from working in certain channels

* Automatic messaging
    - Sends a specified message to a specified channel at a specified time of week
    - Can't yet be changed without a restart- this is being worked on

## Dependencies
[PyYAML](https://pypi.org/project/PyYAML/) - to parse and load configuration files

[Discord.py](https://pypi.org/project/discord/) - to communicate with the Discord API

[APScheduler](https://pypi.org/project/APScheduler/) - to handle automatic messaging in a Cron-like, but cross-platform, manner

## Configuration
`config.yaml` in the root contains the bot token and the ID of the bot owner. See `config.sample.yaml` for a template.

The `config` folder contains server-specific configuration. See `config/sample.yaml` for a template.
